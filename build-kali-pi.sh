#!/bin/bash

export http_proxy=http://localhost:3142/


# Configure these if needed
#################################################################
disksize="3000"
architecture="armel"
packages="xfce4 kali-menu nmap sqlmap"

# Configure these if you know what you are doing.
#################################################################
deb_release="kali"
bootsize="64M"
deb_mirror="http://repo.kali.org/kali"
buildenv="/root/kali-pi"
rootfs="${buildenv}/rootfs"
bootfs="${rootfs}/boot"


# Don't touch these unless you really know what you are doing.
#################################################################
timestamp=`date +%Y%m%d`
device=$1

if [ "$deb_local_mirror" == "" ]; then
  deb_local_mirror=$deb_mirror
fi

image=""


if [ $EUID -ne 0 ]; then
  echo -e "\e[1;34m[*] This tool must be run as root.\e[0m"
  exit 1
fi

if ! [ -b $device ]; then
  echo -e "\e[1;34m[*] $device is not a block device.\e[0m"

  exit 1
fi

if [ "$device" == "" ]; then
  echo -e "\e[1;34m[*] No block device given, just creating an image.\e[0m"

  mkdir -p $buildenv
  image="${buildenv}/${deb_release}_pi_${timestamp}.img"
  dd if=/dev/zero of=$image bs=1MB count=$disksize
  device=`losetup -f --show $image`
  echo -e "\e[1;34m[*] Image $image created and mounted as $device.\e[0m"

else
  dd if=/dev/zero of=$device bs=512 count=1
fi

echo -e "\e[1;34m[*] Partitioning $device.\e[0m"

parted $device --script -- mklabel msdos
parted $device --script -- mkpart primary fat32 0 64
parted $device --script -- mkpart primary ext4 64 -1


if [ "$image" != "" ]; then
  losetup -d $device
  device=`kpartx -va $image | sed -E 's/.*(loop[0-9])p.*/\1/g' | head -1`
  device="/dev/mapper/${device}"
  bootp=${device}p1
  rootp=${device}p2
else
  if ! [ -b ${device}1 ]; then
    bootp=${device}p1
    rootp=${device}p2
    if ! [ -b ${bootp} ]; then
     echo -e "\e[1;34m[*] Something went wrong, can't find bootpartition neither as ${device}1 nor as ${device}p1, exiting.\e[0m"

      exit 1
    fi
  else
    bootp=${device}1
    rootp=${device}2
  fi
fi

echo -e "\e[1;34m[*]  Formatting $bootp.\e[0m"
mkfs.vfat $bootp
echo -e "\e[1;34m[*]  Formatting $rootp.\e[0m"
mkfs.ext4 $rootp
mkdir -p $rootfs

echo -e "\e[1;34m[*] Mounting $rootp to $rootfs.\e[0m"
mount $rootp $rootfs
cd $rootfs

echo -e "\e[1;34m[*] Building $deb_release $architecture in $rootfs from $deb_local_mirror.\e[0m"

debootstrap --foreign --arch $architecture $deb_release $rootfs $deb_local_mirror
cp /usr/bin/qemu-arm-static usr/bin/
echo -e "\e[1;34m[*] Entering 2nd stage chroot.\e[0m"

LANG=C chroot $rootfs /debootstrap/debootstrap --second-stage

mount $bootp $bootfs
echo "deb $deb_local_mirror $deb_release main contrib non-free" > $rootfs/etc/apt/sources.list
echo "dwc_otg.lpm_enable=0 console=ttyAMA0,115200 kgdboc=ttyAMA0,115200 console=tty1 root=/dev/mmcblk0p2 rootfstype=ext4 rootwait" >  $bootfs/cmdline.txt

echo -e "\e[1;34m[*] Fetching r-pi firmware.\e[0m"
git clone git://git.kali.org/packages/raspberry-pi-firmware
cd raspberry-pi-firmware
mv bootcode.bin fixup_cd.dat fixup.dat kernel_emergency.img kernel.img start_cd.elf start.elf $bootfs/
cd ..
rm -rf raspberry-pi-firmware

cat << EOF > $rootfs/etc/fstab
proc            /proc           proc    defaults        0       0
/dev/mmcblk0p1  /boot           vfat    defaults        0       0
EOF

echo "kali" > $rootfs/etc/hostname

echo -e "\e[1;34m[*] Setting up network for DHCP.\e[0m"

cat << EOF > $rootfs/etc/network/interfaces
auto lo
iface lo inet loopback
auto eth0
iface eth0 inet dhcp
EOF

cat << EOF >>$rootfs/etc/modules
vchiq
snd_bcm2835
EOF

cat << EOF > $rootfs/debconf.set
console-common	console-data/keymap/policy	select	Select keymap from full list
console-common	console-data/keymap/full	select	de-latin1-nodeadkeys
EOF

echo -e "\e[1;34m[*] Continuing chroot installation.\e[0m"

cat << EOF > $rootfs/third-stage
#!/bin/bash
debconf-set-selections /debconf.set
rm -f /debconf.set
apt-get update
apt-get -y install git-core binutils ca-certificates

apt-get -y install locales console-common openssh-server less nano git
echo "root:toor" | chpasswd
sed -i -e 's/KERNEL\!=\"eth\*|/KERNEL\!=\"/' /lib/udev/rules.d/75-persistent-net-generator.rules
rm -f /etc/udev/rules.d/70-persistent-net.rules

echo "deb $deb_mirror $deb_release main contrib non-free" > /etc/apt/sources.list
echo "deb http://repo.kali.org/security kali/updates main contrib non-free" >> /etc/apt/sources.list
echo "deb http://repo.kali.org/kali kali-proposed-updates main" >> /etc/apt/sources.list
echo "deb http://repo.kali.org/kali kali-dev main contrib non-free" >> /etc/apt/sources.list

echo -e "\e[1;34m[*] Fetching r-pi kernel modules.\e[0m"

git clone git://git.kali.org/packages/raspberry-pi-modules
cp -rf raspberry-pi-modules/lib/* /lib/
rm -rf raspberry-pi-modules

echo -e "\e[1;34m[*] Installing requested packages: $packages.\e[0m"

apt-get --yes --force-yes install $packages

rm -f /third-stage
EOF

chmod +x $rootfs/third-stage

echo -e "\e[1;34m[*] Cleaning up.\e[0m"

LANG=C chroot $rootfs /third-stage

cat << EOF > $rootfs/cleanup
#!/bin/bash
apt-get update
apt-get clean
rm -f cleanup
EOF
chmod +x $rootfs/cleanup
LANG=C chroot $rootfs /cleanup
cd
echo -e "\e[1;34m[*] Unmounting $bootp and $rootp.\e[0m"
umount $bootp
umount $rootp

if [ "$image" != "" ]; then
  kpartx -d $image
  echo -e "\e[1;34m[*] Created image $image.\e[0m"
fi

echo -e "\e[1;34m[*] Done!\e[0m"
